#!/bin/sh

# Extraction script for:
# Animal Crossing (Europe) (En,Fr,De,Es,It).iso

# Outputs:
# - Balloon Fight (USA).nes
# - Baseball (USA) (GameCube Edition).nes
# - Clu Clu Land (USA) (GameCube Edition).nes
# - Donkey Kong 3 (World).nes
# - Donkey Kong Jr. Math (USA, Europe).nes
# - Donkey Kong Jr. (USA) (GameCube Edition).nes
# - Donkey Kong (USA) (GameCube Edition).nes
# - Excitebike (USA) (GameCube Edition).nes
# - Ice Climber (USA, Europe).nes
# - Legend of Zelda, The (USA) (Rev A) (GameCube Edition).nes
# - Mario Bros. (USA) (GameCube Edition).nes
# - Pinball (Europe) (Rev A).nes
# - Punch-Out!! (Europe).nes
# - Soccer (Europe) (Rev A).nes
# - Super Mario Bros. (World).nes
# - Tennis (USA) (GameCube Edition).nes
# - Wario's Woods (Europe).nes
# - FDS BIOS (Animal Crossing).rom

# Requires: wit, wszst, yaz0dec

# Thanks to /u/I_want_FDS_BIOS on Reddit for their guide on extracting these
# files: https://www.reddit.com/r/emulation/comments/377gug/

romextract()
{
	dependency_wit      || return 1
	dependency_wszst    || return 1
	dependency_yaz0dec  || return 1

	echo "Extracting file from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" \
	--files=+/files/tgc/forest_Eng_Final_PAL50.tgc

	echo "Extracting RARC archive ..."
	# tail for offset, head for file size
	tail -c +27529281 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GAFP/files/tgc/forest_Eng_Final_PAL50.tgc" \
		| head -c +1634240 > "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.arc"

	# Unpack RARC archive
	"$WSZST_PATH" X "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.arc" \
		-d "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d"

	# TODO: Patch BIOS to match verified dump
	echo "Decoding FDS BIOS ..."
	"$YAZ0DEC_PATH" "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs"

	echo "Decoding NES ROMs ..."
	for rom in "$ROMEXTRACT_TMPDIR/$SCRIPTID"/famicom.d/game/01/*.szs; do
		"$YAZ0DEC_PATH" "$rom" > /dev/null 2>&1
	done

	echo "Moving files ..."
	# FDS BIOS
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs 0.rarc" \
		"$SCRIPTID/FDS BIOS (Animal Crossing).rom"
	# NES
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/01_nes_cluclu3.bin.szs 0.rarc" \
		"$SCRIPTID/Clu Clu Land (USA) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/02_usa_balloon.nes.szs 0.rarc" \
		"$SCRIPTID/Balloon Fight (USA).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/03_nes_donkey1_3.bin.szs 0.rarc" \
		"$SCRIPTID/Donkey Kong (USA) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/04_usa_jr_math.nes.szs 0.rarc" \
		"$SCRIPTID/Donkey Kong Jr. Math (USA, Europe).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/05_pal_pinball.nes.szs 0.rarc" \
		"$SCRIPTID/Pinball (Europe) (Rev A).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/06_nes_tennis3.bin.szs 0.rarc" \
		"$SCRIPTID/Tennis (USA) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/07_pal_golfm.nes.szs 0.rarc" \
		"$SCRIPTID/Golf (Unverified).nes" # TODO: Unverified
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/08_pal_punchout.nes.szs 0.rarc" \
		"$SCRIPTID/Punch-Out!! (Europe).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/09_usa_baseball_1.nes.szs 0.rarc" \
		"$SCRIPTID/Baseball (USA) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/10_cluclu_1.qd.szs 0.rarc" \
		"$SCRIPTID/Clu Clu Land D (Unverified).fds" # TODO: Unverified
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/11_usa_donkey3.nes.szs 0.rarc" \
		"$SCRIPTID/Donkey Kong 3 (World).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/12_donkeyjr_1.nes.szs 0.rarc" \
		"$SCRIPTID/Donkey Kong Jr. (USA) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/13_pal_soccer.nes.szs 0.rarc" \
		"$SCRIPTID/Soccer (Europe) (Rev A).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/14_exbike.nes.szs 0.rarc" \
		"$SCRIPTID/Excitebike (USA) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/15_pal_wario.nes.szs 0.rarc" \
		"$SCRIPTID/Wario's Woods (Europe).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/16_usa_icecl.nes.szs 0.rarc" \
		"$SCRIPTID/Ice Climber (USA, Europe).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/17_nes_mario1_2.bin.szs 0.rarc" \
		"$SCRIPTID/Mario Bros. (USA) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/18_smario_0.nes.szs 0.rarc" \
		"$SCRIPTID/Super Mario Bros. (World).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/19_usa_zelda1_1.nes.szs 0.rarc" \
		"$SCRIPTID/Legend of Zelda, The (USA) (Rev A) (GameCube Edition).nes"
	# GBA
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_cluclu.bin.szs" \
		 "$SCRIPTID/Clu Clu Land (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_donkey.bin.szs" \
		 "$SCRIPTID/Donkey Kong (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_donkeyjr.bin.szs" \
		 "$SCRIPTID/Donkey Kong Jr. (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_exbike.bin.szs" \
		 "$SCRIPTID/Excitebike (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_mario.bin.szs" \
		 "$SCRIPTID/Mario Bros. (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_pinball.bin.szs" \
		 "$SCRIPTID/Pinball (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_smario.bin.szs" \
		 "$SCRIPTID/Super Mario Bros. (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_soccer.bin.szs" \
		 "$SCRIPTID/Soccer (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_tennis.bin.szs" \
		 "$SCRIPTID/Tennis (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_balloon.bin.szs" \
		 "$SCRIPTID/Balloon Fight (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_baseball.bin.szs" \
		 "$SCRIPTID/Baseball (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_donkey3.bin.szs" \
		 "$SCRIPTID/Donkey Kong 3 (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_golf.bin.szs" \
		 "$SCRIPTID/Golf (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_icecl.bin.szs" \
		 "$SCRIPTID/Ice Climber (Animal Crossing).mb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_jr_math.bin.szs" \
		 "$SCRIPTID/Donkey Kong Jr. Math (Animal Crossing).mb"

	# TODO: Extract Animal Island ROM if possible?
	#       Most likely inside `foresta.rel.szs` which matches:
	#       grep -rl $(printf "\xEA\x24\xFF\xAE\x51\x69\x9A\xA2\x21\x3D")

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
