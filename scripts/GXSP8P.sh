#!/bin/sh

# Extraction script for:
# Sonic Adventure DX: Director's Cut (Europe).iso

# Outputs:
# - Dr. Robotniks Mean Bean Machine (USA, Europe).gg
# - G-Sonic ~ Sonic Blast (World).gg
# - Sonic Chaos (USA, Europe).gg
# - Sonic Drift 2 (Japan, USA).gg
# - Sonic Drift (Japan) (En).gg
# - Sonic Labyrinth (World).gg
# - Sonic Spinball (USA, Europe).gg
# - Sonic & Tails 2 (Japan).gg
# - Sonic & Tails (Japan) (En).gg
# - Sonic The Hedgehog 2 (World).gg
# - Sonic The Hedgehog - Triple Trouble (USA, Europe).gg
# - Sonic The Hedgehog (World) (Rev 1).gg
# - Tails Adventures (World) (En,Ja).gg
# - Tails no Skypatrol (Japan).gg
# - Sonic Adventure DX Directors Cut - ChaoGC (MB).mb

# Requires: wit prstool

romextract ()
{
	dependency_wit      || return 1
	dependency_prstool  || return 1

	echo "Extracting files from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" \
		--files=+/files/g-sonic.prs \
		--files=+/files/labylin.prs \
		--files=+/files/mbmachin.prs \
		--files=+/files/s-drift2.prs \
		--files=+/files/skypat.prs \
		--files=+/files/sonic2.prs \
		--files=+/files/sonic-ch.prs \
		--files=+/files/sonicdri.prs \
		--files=+/files/sonic.prs \
		--files=+/files/sonictai.prs \
		--files=+/files/sonic_tt.prs \
		--files=+/files/spinball.prs \
		--files=+/files/s-tail2.prs \
		--files=+/files/tailsadv.prs \
		--files=+/files/ChaoGC.bin

	echo "Decompressing ROMs ..."
	for rom in "$ROMEXTRACT_TMPDIR/$SCRIPTID"/P-GXSP/files/*.prs; do
		"$PRSTOOL_PATH" -x "$rom" "$rom"
	done

	echo "Moving ROMs ..."
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/g-sonic.prs" \
		"$SCRIPTID/G-Sonic ~ Sonic Blast (World).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/labylin.prs" \
		"$SCRIPTID/Sonic Labyrinth (World).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/mbmachin.prs" \
		"$SCRIPTID/Dr. Robotnik's Mean Bean Machine (USA, Europe).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/s-drift2.prs" \
		"$SCRIPTID/Sonic Drift 2 (Japan, USA).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/skypat.prs" \
		"$SCRIPTID/Tails no Skypatrol (Japan).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/sonic2.prs" \
		"$SCRIPTID/Sonic The Hedgehog 2 (World).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/sonic-ch.prs" \
		"$SCRIPTID/Sonic Chaos (USA, Europe).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/sonicdri.prs" \
		"$SCRIPTID/Sonic Drift (Japan) (En).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/sonic.prs" \
		"$SCRIPTID/Sonic The Hedgehog (World) (Rev 1).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/sonictai.prs" \
		"$SCRIPTID/Sonic & Tails (Japan) (En).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/sonic_tt.prs" \
		"$SCRIPTID/Sonic & Tails 2 (Japan).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/spinball.prs" \
		"$SCRIPTID/Sonic Spinball (USA, Europe).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/s-tail2.prs" \
		"$SCRIPTID/Sonic The Hedgehog - Triple Trouble (USA, Europe).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/tailsadv.prs" \
		"$SCRIPTID/Tails Adventures (World) (En,Ja).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GXSP/files/ChaoGC.bin" \
		"$SCRIPTID/Sonic Adventure DX Directors Cut - ChaoGC (MB).mb"

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
