# romextract.sh -> non-GNU fork

Made for use on OpenBSD; using bash and coreutils (ghead, gtail etc.)

# Original README.md follows

This is a collection of shell scripts for extracting ROMs and other files of
interest from games. Supported game files are listed in `supported.csv`.

romextract.sh does not come with all the tools required for extracting files.
You will have to download these separately and either install them in your
$PATH or place the binaries in `tools`. Different scripts require different
tools.

## Usage

    $ sh romextract.sh [-SCRIPTID] file...

By default `romextract.sh` will select the appropriate extraction script for a
file using `sha1sum` or `shasum`. A specific script can be forced by adding its
`SCRIPTID` as an option before the path to the file.

### Examples

Extract files from a file:

    $ sh romextract.sh game.iso

Skip SHA-1 check for file by forcing a `SCRIPTID`:

    $ sh romextract.sh -SCRIPTID game.iso

Extract files from multiple files:

    $ sh romextract.sh game1.iso -SCRIPTID game2.exe game3.iso

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
