#!/bin/sh

# romextract - Extract games within games

PROGNAME=$(basename "$0")
SCRIPTDIR=$(dirname "$0")

# Print usage if no arguments given
if [ ! $# -ge 1 ]; then
	echo "Usage: $PROGNAME [-SCRIPTID] file..."
	exit 0
fi

# Check for required commands
for arg in cut grep; do
	command -v "$arg" > /dev/null 2>&1 \
	|| { echo "Command '$arg' not found" >&2; exit 1; }
done

# Use default temporary directory if ROMEXTRACT_TMPDIR is not set
if [ -z "$ROMEXTRACT_TMPDIR" ]; then ROMEXTRACT_TMPDIR=/tmp/romextract; fi

# Create temporary directory
mkdir -p "$ROMEXTRACT_TMPDIR" || exit 1

# Check if necessary files exist
if [ ! -f "$SCRIPTDIR/supported.csv" ]; then
	echo "File $SCRIPTDIR/supported.csv not found" >&2
	exit 1
elif [ ! -f "$SCRIPTDIR/dependencies.sh" ]; then
	"File $SCRIPTDIR/dependencies.sh not found" >&2
	exit 1
elif [ ! -d "$SCRIPTDIR/scripts" ]; then
	echo "Directory $SCRIPTDIR/scripts not found" >&2
	exit 1
fi

# Source dependency functions
# shellcheck source=./dependencies.sh
. "$SCRIPTDIR/dependencies.sh"

# Main loop
ndx=1; while [ "$ndx" -le $# ]; do
	arg=$((ndx))
	eval arg="\$$arg"

	# Detect SCRIPTID
	if [ -f "$arg" ] ; then
		FILE="$arg"

		echo ":: Computing SHA-1 for $FILE ..."
		if command -v sha1sum > /dev/null 2>&1; then
			SHA1=$(sha1sum "$FILE" | grep -o '^\S*')
		elif command -v shasum > /dev/null 2>&1; then
			SHA1=$(shasum -a1 "$FILE" | grep -o '^\S*')
		elif $(which sha1 > /dev/null 2>&1); then
			SHA1=$(sha1 -q "$FILE")
		else
			echo "Could not find supported command for computing SHA-1" \
			     "hashes, try forcing SCRIPTID" >&2
			exit 1
		fi

		# Check SHA-1 length
		if [ "$(printf "%s" "$SHA1" | wc -m)" -ne 40 ]; then
			echo "Got malformed SHA-1, try forcing SCRIPTID" >&2
			exit 1
		fi

		echo ":: Comparing SHA-1 with list of supported files ..."
		GAMEINFO=$(grep -i "$SHA1" "$SCRIPTDIR/supported.csv")

		# Only accept hashes found in supported.csv
		if [ -n "$GAMEINFO" ]; then
			SCRIPTID=$(echo "$GAMEINFO" | grep -io "^[a-z0-9_-]*")
			GAMETITLE=$(echo "$GAMEINFO" | grep -io "\".*\"")

			echo ":: File matches $SCRIPTID: $GAMETITLE"

			# Don't skip next arg
			argincrement=1
		else
			echo "Hash for $FILE not found in supported.csv" >&2
			exit 1
		fi

	# Force SCRIPTID
	elif [ "$(echo "$arg" | cut -c-1)" = "-" ]; then
		# Only accept SCRIPTIDs found in supported.csv
		option=$(echo "$arg" | cut -c2-)
		GAMEINFO=$(grep -i "^$option" "$SCRIPTDIR/supported.csv")

		if [ -n "$option" ] && [ -n "$GAMEINFO" ]; then
			# Check for file in next argument
			FILE=$((ndx+1))
			eval FILE="\$$FILE"

			if [ -f "$FILE" ]; then
				SCRIPTID=$(echo "$GAMEINFO" | grep -io "^$option")
				GAMETITLE=$(echo "$GAMEINFO" | grep -io "\".*\"")

				echo ":: Forcing $SCRIPTID: $GAMETITLE for $FILE"

				# Skip next arg
				argincrement=2
			else
				echo "$FILE is not a file" >&2
				exit 1
			fi
		else
			echo "$arg is not a valid option" >&2
			exit 1
		fi
	else
		echo "$arg is not a valid file or option" >&2
		exit 1
	fi

	# Extract
	if [ -f "$SCRIPTDIR/scripts/$SCRIPTID.sh" ]; then
		mkdir -p "$SCRIPTID" || exit 1
		# shellcheck disable=SC1090
		. "$SCRIPTDIR/scripts/$SCRIPTID.sh"
		romextract
	else
		echo "Script scripts/$SCRIPTID.sh not found" >&2
		exit 1
	fi

	ndx=$((ndx+argincrement))
done

echo ":: Cleaning up ..."
rm -r "$ROMEXTRACT_TMPDIR"

echo ":: Done!"
