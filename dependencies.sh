#!/bin/sh

# Sylverant PRS Tool
dependency_prstool ()
{
	if [ -z "$PRSTOOL_PATH" ]; then
		if [ -f "$SCRIPTDIR/tools/prstool" ]; then
			export PRSTOOL_PATH="$SCRIPTDIR/tools/prstool"
		else
			command -v prstool > /dev/null 2>&1 || \
			{
				echo "Could not find 'prstool' (Sylverant PRS Tool)" >&2
				echo "Please put it in your \$PATH or place the binary in" \
					 "$SCRIPTDIR/tools" >&2
				return 1
			}
			export PRSTOOL_PATH=prstool
		fi
	fi
}

# Python 2
dependency_python2 ()
{
	if [ -z "$PYTHON2_PATH" ]; then
		if [ -f "$SCRIPTDIR/tools/python2" ]; then
			export PYTHON2_PATH="$SCRIPTDIR/tools/python2"
		else
			command -v python2 > /dev/null 2>&1 || \
			{
				echo "Could not find 'python2'" >&2
				echo "Please install it using your package manager or" \
					 "download the installer from www.python.org" >&2
				return 1
			}
			export PYTHON2_PATH=python2
		fi
	fi
}

# snesrestore from vcromclaim
dependency_snesrestore ()
{
	dependency_python2 || return 1
	if [ ! -f "$SCRIPTDIR/tools/snesrestore.py" ]; then
		echo "Could not find 'snesrestore.py'" >&2
		echo "Please place the file in $SCRIPTDIR/tools" >&2
		return 1
	elif [ ! -f "$SCRIPTDIR/tools/brrencode3.py" ]; then
		echo "Could not find 'brrencode3.py'" >&2
		echo "Please place the file in $SCRIPTDIR/tools" >&2
		return 1
	elif [ ! -f "$SCRIPTDIR/tools/lzh8.py" ]; then
		echo "Could not find 'lzh8.py'" >&2
		echo "Please place the file in $SCRIPTDIR/tools" >&2
		return 1
	fi
}

#uCON64
dependency_ucon64 ()
{
	if [ -z "$UCON64_PATH" ]; then
		if [ -f "$SCRIPTDIR/tools/ucon64" ]; then
			export UCON64_PATH="$SCRIPTDIR/tools/ucon64"
		else
			command -v ucon64 > /dev/null 2>&1 || \
			{
				echo "Could not find 'ucon64' (uCON64)" >&2
				echo "Please put it in your \$PATH or place the binary in" \
					 "$SCRIPTDIR/tools" >&2
				return 1
			}
			export UCON64_PATH=ucon64
		fi
	fi
}

# Wiimms ISO Tool
dependency_wit ()
{
	if [ -z "$WIT_PATH" ]; then
		if [ -f "$SCRIPTDIR/tools/wit" ]; then
			export WIT_PATH="$SCRIPTDIR/tools/wit"
		else
			command -v wit > /dev/null 2>&1 || \
			{
				echo "Could not find 'wit' (Wiimms ISO Tool)" >&2
				echo "Please put it in your \$PATH or place the binary in" \
					 "$SCRIPTDIR/tools" >&2
				return 1
			}
			export WIT_PATH=wit
		fi
	fi
}

# Wiimms SZS Tool
dependency_wszst ()
{
	if [ -z "$WSZST_PATH" ]; then
		if [ -f "$SCRIPTDIR/tools/wszst" ]; then
			export WSZST_PATH="$SCRIPTDIR/tools/wszst"
		else
			command -v wszst > /dev/null 2>&1 || \
			{
				echo "Could not find 'wszst' (Wiimms SZS Tool)" >&2
				echo "Please put it in your \$PATH or place the binary in" \
					 "$SCRIPTDIR/tools" >&2
				return 1
			}
			export WSZST_PATH=wszst
		fi
	fi
}

# yaz0dec from szstools
dependency_yaz0dec ()
{
	if [ -z "$YAZ0DEC_PATH" ]; then
		if [ -f "$SCRIPTDIR/tools/yaz0dec" ]; then
			export YAZ0DEC_PATH="$SCRIPTDIR/tools/yaz0dec"
		else
			command -v yaz0dec > /dev/null 2>&1 || \
			{
				echo "Could not find 'yaz0dec' (szstools)" >&2
				echo "Please put it in your \$PATH or place the binary in" \
					 "$SCRIPTDIR/tools" >&2
				return 1
			}
			export YAZ0DEC_PATH=yaz0dec
		fi
	fi
}

# ffmpeg
dependency_ffmpeg ()
{
  if [ -z "$FFMPEG_PATH" ]; then
    if [ -f "$SCRIPTDIR/tools/ffmpeg" ]; then
      export FFMPEG_PATH="$SCRIPTDIR/tools/ffmpeg"
    else
      command -v ffmpeg > /dev/null 2>&1 || \
      {
        echo "Could not find 'ffmpeg'" >&2
        echo "Please install it using your package manager" >&2
        return 1
      }
      export FFMPEG_PATH=ffmpeg
    fi
  fi
}

