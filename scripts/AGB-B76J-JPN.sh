#!/bin/sh

# Extraction script for:
# Hudson Best Collection Vol. 6 - Bouken-jima Collection (Japan).gba

# Outputs:
# - Takahashi Meijin no Bouken-jima (Japan) (Hudson Best Collection) (Broken).nes
# - Takahashi Meijin no Bouken-jima II (Japan) (Hudson Best Collection) (Unverified).nes
# - Takahashi Meijin no Bouken-jima III (Japan) (Hudson Best Collection) (Unverified).nes
# - Takahashi Meijin no Bouken-jima IV (Japan) (Hudson Best Collection) (Unverified).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	# TODO: Takahashi Meijin no Bouken-jima has a broken title screen, otherwise works?
	tail -c +229961 "$FILE" | head -c +65552 \
		> "$SCRIPTID/Takahashi Meijin no Bouken-jima (Japan) (Hudson Best Collection) (Broken).nes"
	tail -c +295513 "$FILE" | head -c +262160 \
		> "$SCRIPTID/Takahashi Meijin no Bouken-jima II (Japan) (Hudson Best Collection) (Unverified).nes"
	tail -c +557673 "$FILE" | head -c +262160 \
		> "$SCRIPTID/Takahashi Meijin no Bouken-jima III (Japan) (Hudson Best Collection) (Unverified).nes"
	tail -c +819833 "$FILE" | head -c +393232 \
		> "$SCRIPTID/Takahashi Meijin no Bouken-jima IV (Japan) (Hudson Best Collection) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
