# Links to tools used by romextract.sh scripts

* [Sylverant PRS Tool](https://sourceforge.net/p/sylverant/pso_tools/ci/master/tree/prstool/):
`prstool` (Source code only, requires compilation)
* [szstools](http://amnoid.de/gc/): `yaz0dec` (.zip only has binaries for
Windows, comes with source code and compiles on Linux/Mac)
* [uCON64](http://ucon64.sourceforge.net/): `ucon64`
* [vcromclaim](https://github.com/Plombo/vcromclaim): `brrencode3.py`
`snesrestore.py` `lzh8.py` (requires Python 2, install using your package
manager or download the installer from www.python.org)
* [Wiimms ISO Tools](http://wit.wiimm.de/): `wit`
* [Wiimms SZS Toolset](http://szs.wiimm.de/): `wszst`
