#!/bin/sh

# Extraction script for:
# Shadowgate: MacVenture Series (shadowgate.exe)

# Outputs:
# - Shadowgate.2mg
# - Shadowgate.dsk

romextract()
{

	echo "Extracting game files from shadowgate.exe ..."
	# tail for offset, head for game file size
	tail -c +655521 "$FILE" | head -c +819274 \
		> "$SCRIPTID/Shadowgate.2mg"
	tail -c +1474801 "$FILE" | head -c +839680 \
		> "$SCRIPTID/Shadowgate.dsk"

	echo "Script $SCRIPTID.sh done"
}
