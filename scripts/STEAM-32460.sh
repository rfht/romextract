#!/bin/sh

# Extraction script for:
# Monkey Island 2 Special Edition: LeChuck’s Revenge (Monkey2.pak)

# Outputs:
# - Monkey Island 2: LeChuck's Revenge (DOS/English)

romextract()
{
	if [ -f "$(dirname "$FILE")/monkey2.pak" ]; then
		FILE="$(dirname "$FILE")/monkey2.pak"
	else
		echo "Could not find monkey2.pak"
		return 1
	fi

	echo "Extracting game files from monkey2.pak ..."
	# gtail for offset, ghead for game file size
	gtail -c +1391289 "$FILE" | ghead -c +11135 \
		> "$SCRIPTID/monkey2.000"
	gtail -c +1402424 "$FILE" | ghead -c +9080329 \
		> "$SCRIPTID/monkey2.001"

	echo "Script $SCRIPTID.sh done"
}
