#!/bin/sh

# Extraction script for:
# Legend of Zelda, The - Collector's Edition (Europe) (En,Fr,De,Es,It).iso

# Outputs:
# - Legend of Zelda, The (USA) (Rev B) (GameCube Edition).nes
# - Legend of Zelda, The - Majora's Mask (Europe) (GameCube Edition).z64
# - Legend of Zelda, The - Ocarina of Time (Europe) (GameCube Edition).z64
# - Legend of Zelda, The - Ocarina of Time Credits (Europe) (GameCube Edition).thp
# - Legend of Zelda, The - The Wind Waker Demo (Europe).tgc
# - Zelda II - The Adventure of Link (USA) (GameCube Edition).nes

# Requires: wit

romextract()
{
	dependency_wit || return 1

	echo "Extracting files from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" \
		--files=+/files/tgc/EUR_NES_ZELDA1.tgc \
		--files=+/files/tgc/EUR_NES_ZELDA2.tgc \
		--files=+/files/tgc/majora_PAL_100403c.tgc \
		--files=+/files/tgc/zelda_PAL_093003.tgc \
		--files=+/files/tgc/ZL_WindWakerPALSHOP_final_2003-09-26_16-45-37.tgc

	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +542785 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLP/files/tgc/EUR_NES_ZELDA1.tgc" \
		| head -c +131088 > "$SCRIPTID/Legend of Zelda, The (USA) (Rev B) (GameCube Edition).nes"
	tail -c +542529 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLP/files/tgc/EUR_NES_ZELDA2.tgc" \
		| head -c +262160 > "$SCRIPTID/Zelda II - The Adventure of Link (USA) (GameCube Edition).nes"
	tail -c +29630401 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLP/files/tgc/majora_PAL_100403c.tgc" \
		| head -c +33554432 > "$SCRIPTID/Legend of Zelda, The - Majora's Mask (Europe) (GameCube Edition).z64"
	tail -c +476487617 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLP/files/tgc/zelda_PAL_093003.tgc" \
		| head -c +33554432 > "$SCRIPTID/Legend of Zelda, The - Ocarina of Time (Europe) (GameCube Edition).z64"
	tail -c +2441525 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLP/files/tgc/zelda_PAL_093003.tgc" \
		| head -c +466727296 > "$SCRIPTID/Legend of Zelda, The - Ocarina of Time Credits (Europe) (GameCube Edition).thp"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLP/files/tgc/ZL_WindWakerPALSHOP_final_2003-09-26_16-45-37.tgc" \
		"$SCRIPTID/Legend of Zelda, The - The Wind Waker Demo (Europe).tgc"

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
